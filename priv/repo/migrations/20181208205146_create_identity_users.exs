defmodule Banana.Repo.Migrations.CreateIdentityUsers do
  use Ecto.Migration

  def change do
    create table(:identity_users) do
      add :username, :string
      add :first_name, :string
      add :last_name, :string
      add :email, :string

      timestamps()
    end

  end
end
