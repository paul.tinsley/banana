# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :banana,
  ecto_repos: [Banana.Repo]

# Configures the endpoint
config :banana, BananaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "3m8tz8z4bA6OmPc+uSOBbtAFvtMJfjG8xryaoTuXVukL/FRZ9qL2RSvtjcUtoIqZ",
  render_errors: [view: BananaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Banana.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
