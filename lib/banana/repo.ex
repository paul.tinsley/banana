defmodule Banana.Repo do
  use Ecto.Repo,
    otp_app: :banana,
    adapter: Ecto.Adapters.Postgres
end
