defmodule Banana.Identity.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "identity_users" do
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :first_name, :last_name, :email])
    |> validate_required([:username, :first_name, :last_name, :email])
  end
end
